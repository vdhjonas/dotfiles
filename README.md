dotfiles
========

My personal dotfiles. Also used by nearly all other geeks at [uni-t.be](http://uni-t.be) 

You can install them by cloning the repository as '.dotfiles' in your homedirectory and running the bootstrap-script.

The bootstrap script can be run be cd-ing into the .dotfiles directory and performing this command:
```bash
./bootstrap
```

Feel free to contact me if you have any questions about them.

# What does it do?
##uninstalling 
1) apache on your iOS, interfering with Mamp Pro

##Installing
- homebrew

- wget

- [httpie](https://httpie.org/) 

- [ncdu](https://lintut.com/ncdu-check-disk-usage/)

- [brew-cask](https://github.com/caskroom/homebrew-cask)
###[Documentation](http://blog.kilian.io/brew-cask/)

- 1Password

- Alfred 3
- Balsamiq mockups
- Adobe Creative Cloud
- Photoshop CC
- Illustrator CC
- Camtasia
- Daisydisk
- Mamp Pro 4

- Redis desktop manager
- Redis app

- Sequel Pro
- Sketch
- Skype
- Slack
- Teamviewer

#### Browsers
- Blisk
- Chrome
- Firefox

#### Editors
- Sublime Text 3
- Phpstorm

#### Git clients
- Tower
- Sourcetree
